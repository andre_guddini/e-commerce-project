const hamburger = document.querySelector('.hamburger');
const navMenu = document.querySelector('.nav-menu');
const navigationLinks = document.querySelectorAll('.nav-link');


hamburger.addEventListener('click', ()=>{
    hamburger.classList.toggle('active');
    navMenu.classList.toggle('active');
})

navigationLinks.forEach(link => link.addEventListener('click' ,() =>{
    hamburger.classList.remove('active');
    navMenu.classList.remove('active');
}))