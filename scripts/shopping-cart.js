const cartBtn = document.querySelector('.shopping-cart-icon');
const cartModal = document.querySelector('.product-cart');
const closeCartBtn = document.querySelector('.closeCartModal');
const continueShoppingBtn = document.querySelector('.continue-shopping');
const totalPrice = document.querySelector('.cart-total-price');



cartBtn.addEventListener('click',openCart);

function openCart(){
    cartModal.classList.toggle('hidden');
    if(!loginModal.classList.contains('hidden')){  // from login-window file
        loginModal.classList.add('hidden');
    }
}

closeCartBtn.addEventListener('click', closeCart);
continueShoppingBtn.addEventListener('click', closeCart);

function closeCart(){
    if(!cartModal.classList.contains('hidden')){
        cartModal.classList.add('hidden');
    }
}

let basket = JSON.parse(localStorage.getItem("product-data")) ||[];  //якщо локал сторедж пустий, то значення basket- пустий масив

const increment = (id) =>{
 let selectedIted = id;
 let search = basket.find((x) => x.id ===  selectedIted)               //чи є дублікати в масиві basket selectedItem.id після рефактору

 if(search === undefined){
    basket.push({
        id: selectedIted,               
        quantity:1,
    });
 } else{
     search.quantity += 1;
 }
 localStorage.setItem("product-data", JSON.stringify(basket))
 update(selectedIted);
}
const decrement = (id) => {
    let selectedIted = id;
    let search = basket.find((x) => x.id ===  selectedIted)               //чи є дублікати в масиві basket selectedItem.id після рефактору
    if(search.quantity === 0){
        return;
    } else{
        search.quantity -= 1;
    }
    localStorage.setItem("product-data", JSON.stringify(basket))
    update(selectedIted);  
}

const remove= (id) =>{
    let updatedProduct = JSON.parse(localStorage.getItem("product-data"));
    let updatedProductData = updatedProduct.filter(obj => obj.id !== id); // всі елементи, окрім того, який ми видалили
    localStorage.setItem("product-data", JSON.stringify(updatedProductData)); //оновлення локал стореджу
    basket = JSON.parse(localStorage.getItem("product-data"))
    document.getElementById(`cart-${id}`).remove();
    updateCartNumber();
    updateTotalCost();

}

const update = (id) => {           //це кількість окремого предмета в корзині
    let search = basket.find((x) =>x.id === id);
    
    document.getElementById(id).innerHTML = search.quantity;
    if(search.quantity == 0){
        remove(id);
    }else{
        updatePrice(id);
        updateCartNumber();
    }
    checkIfCartEmpty();
}

const updateCartNumber =() => {         // це загальна кількість предметів в корзині
    const cartIcon = document.querySelector('.cartSum');
    cartIcon.innerText = basket.map((x)=>x.quantity).reduce((x,y) =>x+y,0) 
     //мап відображає масив з числами(quantity), reduce додає всі числа
}

const updatePrice = (id) =>{
    const cartItem = document.querySelector(`#cart-${id}`);
    let cartItemPrice = cartItem.querySelector('.cart-product-price');
    let search = basket.find((x) =>x.id === id);
    cartItemPrice.innerText = `${(search.quantity * search.price).toFixed(2)}$`;
    updateTotalCost();
}

const updateTotalCost = () =>{
    totalProductPrice = basket.map((x)=>x.quantity*x.price).reduce((x,y) =>x+y,0); //додавання ціни предмета помноженого на якого кількість по всіх предметах
    totalPrice.innerText = `Total Price is: ${totalProductPrice.toFixed(2)}$`;
}

updateTotalCost(); //щоб коли перезагрузивши сторінку показувало загальну ціну 
updateCartNumber(); //щоб коли перезагрузивши сторінку показувало кількість предметів в корзині