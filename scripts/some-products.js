// деякі функції такі ж як в файлі products.js, однак зі змінами, відповідно до специфіки окремого товару

const productDetails = document.querySelector('#product-details');
let productData;

const checkIfCartEmpty = () =>{
    const productItems = document.querySelector('.product-items');
    const productItem = productItems.querySelectorAll('.product-item');
    const emptyCartDescr = document.querySelector('.empty-cart-descr')
    if (productItem.length === 0) {
        emptyCartDescr.style.display = 'block';
     } else {
       emptyCartDescr.style.display = 'none';
     }
}

async function logProductsData(url) {
    try {
        const responce = await fetch(url);
        const jsonData = await responce.json();
        function generateCartItems(){
            const productItems = document.querySelector('.product-items');
            if (basket.length> 0){              //basket from shopping-cart file
                jsonData.map((x) =>{
                    let {id,image,title,quantity, price} = x;
                    let existingItem = basket.find((x) => x.id === id) ||[];
                    let cartProductHtml = `
                    <div class="product-item" id="cart-${id}">
                    <div class="cart-product-descr">
                        <div class="cart-product-img"
                            style="background-image: url(${image});">
                        </div>
                        <div class="cart-product-title">${title}</div>
                    </div>
                    <div class="cart-product-quantity-wrapper">
                        <img src="./images/minus.png" alt="minus-product-button" onclick="decrement(${id})">
                        <div class="cart-product-quantity" id="${id}">${existingItem.quantity === undefined? 1 :
                            existingItem.quantity
                          }</div>
                        <img src="./images/add.png" alt="add-product-button" onclick="increment(${id})">
                    </div>
                    <div class="cart-product-remove">
                        <img src="./images/remove.png" alt="remove-product-button" onclick="remove(${id})">
                    </div>
                    <div class="cart-product-price-wrapper">
                        <p class="cart-product-price">${price}</p>
                    </div>
                </div>`;
                    
                    if(existingItem.id == id){
                        productItems.insertAdjacentHTML('beforeend', cartProductHtml);
                        updatePrice(id);                        
                    }
                    checkIfCartEmpty();
                })
              
            }
        } 
        generateCartItems();
        }catch (error) {
        const errorHtml = `<div class='products-error'>Can't load products information, try again later</div>`
        productDetails.insertAdjacentHTML('beforeend', errorHtml);
        console.error(error);
    }

}
logProductsData('https://fakestoreapi.com/products/');


