const productGrid = document.querySelector('.products-grid');
const sortedProductsGrid = document.querySelector('.products-sorted-grid')
let productData;

const checkIfCartEmpty = () =>{
    const productItems = document.querySelector('.product-items');
    const productItem = productItems.querySelectorAll('.product-item');
    const emptyCartDescr = document.querySelector('.empty-cart-descr')
    if (productItem.length === 0) {
        emptyCartDescr.style.display = 'block';
     } else {
       emptyCartDescr.style.display = 'none';
     }
}


async function logProductsData(url) {
    const loader = document.querySelector('.loader-modal');
    loader.classList.remove('hide');
    try {
        const responce = await fetch(url);
        const jsonData = await responce.json();
        jsonData.forEach(element => {
            let gridHtml = `
        <div class="grid_item">
        <div class="grid_image" style="background-image: url(${element.image})" onclick="openProductPage('${element.id}')">
        <div class="grid_descr"><p>Product page</p></div>
        </div>
        <div class="grid_body">
        <div class="grid_title"><p><strong>${element.title}</strong></p></div>
        <div class="grid_price"><p><strong>Price:</strong> ${element.price}$</p></div>
        <div class="grid_category"><p><strong>Category:</strong> ${element.category}</p></div>
        <button type="button" class="add-to-cart" id="btn-${element.id}">Add to cart </button>
       </div>
        </div>`
            loader.classList.add('hide');
            productGrid.insertAdjacentHTML('beforeend', gridHtml);
        });

        function productSort() {  //для сортування товарів за категоріями
            const productSortBtn = document.querySelectorAll('.product-category');

            productSortBtn.forEach(product => {
                product.addEventListener('click', () => {
                    const isActive = product.classList.contains('active');
                    productSortBtn.forEach(category => {
                        category.classList.remove('active');    //щоб забрати активний класс з попередньо натиснутих кнопок
                    });
                    if (!isActive) {
                        product.classList.add('active');
                    }
                    if (isActive) {
                        product.classList.remove('active'); // щоб при 2 кліці класу не було 
                    }
                    const categoryName = product.querySelector('p').textContent; //назва категорії
                    console.log(categoryName)
                    const sortedProducts = [];
                    jsonData.forEach(element => {
                        if (element.category === categoryName.toLowerCase() && !isActive) {
                            sortedProducts.push(element);

                        }
                    })
                    if (!isActive) {
                        sortedProductsGrid.textContent = ''; //щоб видалити старі результати сорту
                        sortedProducts.forEach(element => {
                            let sortedgridHtml = `
                        <div class="grid_item">
                        <div class="grid_image" style="background-image: url(${element.image})" onclick="openProductPage('${element.id}')">
                        <div class="grid_descr"><p>Product page</p></div>
                        </div>
                        <div class="grid_body">
                        <div class="grid_title"><p><strong>${element.title}</strong></p></div>
                        <div class="grid_price"><p><strong>Price:</strong> ${element.price}$</p></div>
                        <div class="grid_category"><p><strong>Category:</strong> ${element.category}</p></div>
                        <button type="button" class="add-to-cart" id="btn-${element.id}">Add to cart </button>
                       </div>
                        </div>`
                            sortedProductsGrid.insertAdjacentHTML('beforeend', sortedgridHtml);
                            productGrid.style.display = 'none';
                            sortedProductsGrid.style.display = 'grid';
                            const addToCartBtn = document.querySelectorAll('.add-to-cart');
                            addToCartBtn.forEach(btn => {
                                btn.addEventListener('click', addToCart);
                            })
                        });
                    }
                    if (isActive) {
                        sortedProductsGrid.textContent = ''; //можна innerHTML
                        productGrid.style.display = 'grid';
                    }
                });
            });


        }
        function generateCartItems() { //для генерації корзини
            const productItems = document.querySelector('.product-items');
            if (basket.length > 0) {              //basket from shopping-cart file
                jsonData.map((x) => {
                    let { id, image, title, quantity, price } = x;
                    let existingItem = basket.find((x) => x.id === id) || [];
                    let cartProductHtml = `
                    <div class="product-item" id="cart-${id}">
                    <div class="cart-product-descr">
                        <div class="cart-product-img"
                            style="background-image: url(${image});">
                        </div>
                        <div class="cart-product-title">${title}</div>
                    </div>
                    <div class="cart-product-quantity-wrapper">
                        <img src="./images/minus.png" alt="minus-product-button" onclick="decrement(${id})">
                        <div class="cart-product-quantity" id="${id}">${existingItem.quantity === undefined ? 1 :
                            existingItem.quantity
                        }</div>
                        <img src="./images/add.png" alt="add-product-button" onclick="increment(${id})">
                    </div>
                    <div class="cart-product-remove">
                        <img src="./images/remove.png" alt="remove-product-button" onclick="remove(${id})">
                    </div>
                    <div class="cart-product-price-wrapper">
                        <p class="cart-product-price">${price}</p>
                    </div>
                </div>`;
                    
                    if (existingItem.id == id && existingItem.quantity > 0) {
                        productItems.insertAdjacentHTML('beforeend', cartProductHtml);
                        updatePrice(id);
                    }
                   checkIfCartEmpty();
                   
                })
            }
            
        }
        generateCartItems();
        const addToCartBtn = document.querySelectorAll('.add-to-cart');
        addToCartBtn.forEach(btn => {
            btn.addEventListener('click', addToCart);
        })
        function addToCart() {
            const productItems = document.querySelector('.product-items');
            const btnId = this.id;
            const productId = +btnId.substring(btnId.indexOf("-") + 1);     //to get 1 instead of btn-1
            let increment = (id) => {
                let selectedIted = +id;
                let search = basket.find((x) => x.id === selectedIted)               //чи є дублікати в масиві basket selectedItem.id після рефактору
                if (search === undefined) {
                    jsonData.map(element => {
                        let search = basket.find((x) => x.id === element.id) || [];
                        if (productId == element.id) {
                            let cartProductHtml = `
                        <div class="product-item" id="cart-${element.id}">
                        <div class="cart-product-descr">
                            <div class="cart-product-img"
                                style="background-image: url(${element.image});">
                            </div>
                            <div class="cart-product-title">${element.title}</div>
                        </div>
                        <div class="cart-product-quantity-wrapper">
                            <img src="./images/minus.png" alt="minus-product-button" onclick="decrement(${element.id})">
                            <div class="cart-product-quantity" id="${element.id}">${search.quantity === undefined ? 1 :
                                    search.quantity
                                }</div>
                            <img src="./images/add.png" alt="add-product-button" onclick="increment(${element.id})">
                        </div>
                        <div class="cart-product-remove">
                            <img src="./images/remove.png" alt="remove-product-button" onclick="remove(${element.id})">
                        </div>
                        <div class="cart-product-price-wrapper">
                            <p class="cart-product-price">${element.price}</p>
                        </div>
                    </div>`;
                            productItems.insertAdjacentHTML('beforeend', cartProductHtml);
                            basket.push({
                                id: selectedIted,
                                quantity: 1,
                                price: element.price
                            });
                        }
                    }
                    )
                } else {
                    search.quantity += 1;
                }
                localStorage.setItem("product-data", JSON.stringify(basket))
                update(selectedIted);
            }



            increment(productId);// з файлу shopping-cart
        }
        productSort();
    } catch (error) {
        const errorHtml = `<div class='products-error'>Can't load products information, try again later</div>`
        loader.classList.add('hide');
        productGrid.insertAdjacentHTML('beforeend', errorHtml);
        console.error(error);
    }

}
logProductsData('https://fakestoreapi.com/products/');


function openProductPage(productId) {
    const productPageUrl = `product.html?id=${productId}`;
    window.open(productPageUrl, "_blank");
}