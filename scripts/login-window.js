const loginBtn = document.querySelector('.login-icon');
const loginModal = document.querySelector('#login-modal');
const closeLoginModal = document.querySelector('.closeLoginModal')
const cancelLoginModalBtn = document.querySelector('.cancelLoginModalBtn')


loginBtn.addEventListener('click',openLoginForm);

function openLoginForm(){
    if(!cartModal.classList.contains('hidden')){
        cartModal.classList.add('hidden');
    }
  loginModal.classList.toggle('hidden');
}

closeLoginModal.addEventListener('click', closeLoginForm);

function closeLoginForm(){
    if(!loginModal.classList.contains('hidden')){
        loginModal  .classList.add('hidden');
    }
   
}

cancelLoginModalBtn.addEventListener('click', closeLoginForm);