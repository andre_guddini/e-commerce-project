function imageZoom(imgID){
      const img = document.getElementById(imgID);
      const lens = document.getElementById('lens')
      console.log(lens)
      img.addEventListener('mousemove', showLens);
      function showLens(){
          lens.classList.remove('hidden')
      }
      lens.style.backgroundImage = `url(${img.src})`

      let ratio = 2;

      lens.style.backgroundSize = (img.width * ratio) + 'px '+
      (img.height * ratio) + 'px';
      
      img.addEventListener('mousemove', moveLens);
      lens.addEventListener('mousemove', moveLens);
      img.addEventListener('touchmove', moveLens);  //для телефонів

      function moveLens(){
        let position = getCursor();//позиція курсору відносно зображення
        let positionLeft = position.x - (lens.offsetWidth/3);  //можна просто position x затестити 
        let positionTop = position.y - (lens.offsetHeight /3);
        if(positionLeft < -50){
            positionLeft = -50;
            lens.classList.add('hidden')
        }
        if(positionTop< -50){
            positionTop = -50;
            lens.classList.add('hidden')
        }
        if(positionLeft > img.width - lens.offsetWidth/1.5){
            positionLeft = img.width - lens.offsetWidth/1.5;//довжина картинки+ лінзи 
            lens.classList.add('hidden')  
        }
        if(positionTop > img.height - lens.offsetHeight/2){
            positionTop = img.height - lens.offsetHeight/2;
            lens.classList.add('hidden')
        }
        lens.style.left = positionLeft + 'px';
        lens.style.top = positionTop + 'px';
        lens.style.backgroundPosition = '-' + (position.x * ratio) + 'px -'+
        (position.y * ratio) + 'px';
      }

      function getCursor(){
          const e = window.event;
          let bounds = img.getBoundingClientRect();//відношення блоку з картинкою до верху і лівої сторони екрану
          let x = e.pageX - bounds.left;
          let y = e.pageY - bounds.top;
          x = x - window.scrollX;
          y = y - window.scrollY;
          return {'x':x, 'y':y}
      }
}

