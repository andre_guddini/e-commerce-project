const buttons = document.querySelectorAll("[data-carousel-button]");

buttons.forEach(button => {
    button.addEventListener('click', () => {
        const offset = button.dataset.carouselButton === 'next' ? 1 : -1;  //значення буде значення властивості data-carousel-button //тобто next або prev 
        const slides = button.closest('[data-carousel]').querySelector('[data-slides]');  //closest parent element to buttons with [data-carousel] data tag
        // потім з [data-carousel] ми шукаємо вкладений елемент з дата тегом [data-slides]
       
       const activeSlide = slides.querySelector('[data-active]');
       let newIndex = [...slides.children].indexOf(activeSlide) + offset;//шукаємо в псевдомасиві зі слайдами індекс активного слайду
       if(newIndex <0){
        newIndex = slides.children.length -1 //щоб натискаючи  кнопку попереднього слайду на першому слайді переключалось на останній
       } 
       if(newIndex >= slides.children.length){
           newIndex = 0;  //щоб з останнього слайду на перший кидало, якщо кнопку наступного слайду натискаєш   
       }

       slides.children[newIndex].dataset.active = true; //щоб додати дата атрибут до нового слайду(на який ми переключились)
       delete activeSlide.dataset.active; //щоб видалити його з старого
    })                                          
})