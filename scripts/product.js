const urlParams = new URLSearchParams(window.location.search); //'?id=13' result
    const productId = urlParams.get("id"); //13
    
    //відображення інформації окремого товару, на сторінці окремого товару

    async function logProductInfo() {
      try {
        const response = await fetch(`https://fakestoreapi.com/products/${productId}`);
        const product = await response.json();
        const productDetailsContainer = document.getElementById("product-details");
        let productHtml = `
          <div class='product-container'>
            <div class='product-image'>
              <div id="lens" class="hidden"> </div>
              <img src="${product.image}" alt="${product.title}" id="product-image">
            </div>
            <div class='product-description'>
              <h1>${product.title}</h1>
              <p>${product.description}</p>
              <p class="price">Price: ${product.price}$</p>
              <p class="category">Category: ${product.category}</p>
              <button type="button" class="add-to-cart"id="btn-${product.id}">Add to cart </button>
            </div>
          </div>
        `;
        productDetailsContainer.insertAdjacentHTML('beforeend', productHtml);
        imageZoom('product-image');//function from image-zoom.js file
       
        const addToCartBtn = document.querySelector('.add-to-cart');
        addToCartBtn.addEventListener('click', addToCart); 
      
      function addToCart() {
          const productItems = document.querySelector('.product-items');
          const btnId = this.id;
          const productId = +btnId.substring(btnId.indexOf("-") + 1);     //to get 1 instead of btn-1
          let increment = (id) => {
              let selectedIted = +id;
              let search = basket.find((x) => x.id === selectedIted)               //чи є дублікати в масиві basket selectedItem.id після рефактору
              if (search === undefined) {
                   let search = basket.find((x) => x.id === product.id) || [];
                      if (productId == product.id) {
                          let cartProductHtml = `
                      <div class="product-item" id="cart-${product.id}">
                      <div class="cart-product-descr">
                          <div class="cart-product-img"
                              style="background-image: url(${product.image});">
                          </div>
                          <div class="cart-product-title">${product.title}</div>
                      </div>
                      <div class="cart-product-quantity-wrapper">
                          <img src="./images/minus.png" alt="minus-product-button" onclick="decrement(${product.id})">
                          <div class="cart-product-quantity" id="${product.id}">${search.quantity === undefined ? 1 :
                                  search.quantity
                              }</div>
                          <img src="./images/add.png" alt="add-product-button" onclick="increment(${product.id})">
                      </div>
                      <div class="cart-product-remove">
                          <img src="./images/remove.png" alt="remove-product-button" onclick="remove(${product.id})">
                      </div>
                      <div class="cart-product-price-wrapper">
                          <p class="cart-product-price">${product.price}</p>
                      </div>
                  </div>`;
                          productItems.insertAdjacentHTML('beforeend', cartProductHtml);
                          basket.push({
                              id: selectedIted,
                              quantity: 1,
                              price: product.price
                          });
                      }
                 
              } else {
                  search.quantity += 1;
              }
              localStorage.setItem("product-data", JSON.stringify(basket))
              update(selectedIted);
          }
          increment(productId);// з файлу shopping-cart
      }

      } catch (error) {
        console.error(error);
        const productDetailsContainer = document.getElementById("product-details");
        productDetailsContainer.style.marginTop='80px'
        productDetailsContainer.innerText = "Error loading product details";
      }
    }
    logProductInfo();